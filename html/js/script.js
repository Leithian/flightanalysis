(function($){
    $(function(){
  
      $('.sidenav').sidenav();
      $('.parallax').parallax();
      $('.collapsible').collapsible();
  
    }); // end of document ready
  })(jQuery); // end of jQuery name space



$('.counter').each(function() {
    var $this = $(this),
        countTo = $this.attr('data-count');
    
    $({ countNum: $this.text()}).animate({
      countNum: countTo
    },
  
    {
  
      duration: 5000,
      easing:'linear',
      step: function() {
        $this.text(Math.floor(this.countNum));
      },
      complete: function() {
        $this.text(this.countNum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
      }
  
    });  
    
    
  
  });

/**
 * Charts
 */

  // Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

var chart1 = createChart("chartdiv1", [22, 1, 1307489, 21901]);
var chart2 = createChart("chartdiv2", [6093, 4500, 1301380, 17440]);

function createChart(divName, data) {
  let chart = am4core.create( divName, am4charts.XYChart );
  chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

  chart.maskBullets = false;

  var xAxis = chart.xAxes.push( new am4charts.CategoryAxis() );
  var yAxis = chart.yAxes.push( new am4charts.CategoryAxis() );

  xAxis.dataFields.category = "x";
  yAxis.dataFields.category = "y";

  var xVal = chart.xAxes.push( new am4charts.ValueAxis() );
  var yVal = chart.yAxes.push( new am4charts.ValueAxis() );

  xVal.title.text = "Vorhersage";
  xVal.title.fontSize = 16;
  yVal.title.text = "Real";
  yVal.title.fontSize = 16;

  xAxis.renderer.grid.template.disabled = true;
  xAxis.renderer.minGridDistance = 40;

  yAxis.renderer.grid.template.disabled = true;
  yAxis.renderer.inversed = true;
  yAxis.renderer.minGridDistance = 30;

  var series = chart.series.push( new am4charts.ColumnSeries() );
  series.dataFields.categoryX = "x";
  series.dataFields.categoryY = "y";
  series.dataFields.value = "value";
  series.sequencedInterpolation = true;
  series.defaultState.transitionDuration = 3000;

  // Set up column appearance
  var column = series.columns.template;
  column.strokeWidth = 2;
  column.strokeOpacity = 1;
  column.stroke = am4core.color( "#ffffff" );
  column.tooltipText = "{x}, {y}: {value.workingValue.formatNumber('#.')}";
  column.width = am4core.percent( 100 );
  column.height = am4core.percent( 100 );
  column.column.cornerRadius(6, 6, 6, 6);
  column.propertyFields.fill = "color";

  var bullet2 = series.bullets.push(new am4charts.LabelBullet());
  bullet2.label.text = "{value}";
  bullet2.label.fill = am4core.color("#fff");
  bullet2.zIndex = 1;
  bullet2.fontSize = 15;
  bullet2.interactionsEnabled = false;

  // define colors
  var colors = {
    "critical": chart.colors.getIndex(0).brighten(0.5),
    "medium": chart.colors.getIndex(1).brighten(-0.4),
    "verygood": chart.colors.getIndex(1).brighten(0,2)
  };

  chart.data = [ {
    "y": "kein Ausfall",
    "x": "Ausfall",
    "color": colors.medium,
    "value": data[0]
  }, {
    "y": "Ausfall",
    "x": "Ausfall",
    "color": colors.verygood,
    "value": data[1]
  },
  
  {
    "y": "kein Ausfall",
    "x": "kein Ausfall",
    "color": colors.critical,
    "value": data[2]
  }, {
    "y": "Ausfall",
    "x": "kein Ausfall",
    "color": colors.medium,
    "value": data[3]
  }
  ];

  return chart;
}


/* stackedColumn */
  
  // Create chart instance
  var chart3 = am4core.create("stackedColumn", am4charts.XYChart);
  
  
  // Add data
  chart3.data = [{
    "test": "Test 1",
    "true": 1,
    "false": 21901,
  }, {
    "test": "Test 2",
    "true": 4500,
    "false": 17440,
  }];
  
  // Create axes
  var chart3CategoryAxis = chart3.xAxes.push(new am4charts.CategoryAxis());
  chart3CategoryAxis.dataFields.category = "test";
  chart3CategoryAxis.renderer.grid.template.location = 0;
  
  
  var chart3ValueAxis = chart3.yAxes.push(new am4charts.ValueAxis());
  chart3ValueAxis.renderer.inside = true;
  chart3ValueAxis.renderer.labels.template.disabled = true;
  //valueAxis.logarithmic = true;
  chart3ValueAxis.extraMax = 0.05;
  chart3ValueAxis.renderer.minGridDistance = 20;
  chart3ValueAxis.min = 1;
  
  // Create series
  function createSeries(field, name) {
    
    // Set up series
    var series = chart3.series.push(new am4charts.ColumnSeries());
    series.name = name;
    series.dataFields.valueY = field;
    series.dataFields.categoryX = "test";
    series.sequencedInterpolation = true;
    
    // Make it stacked
    series.stacked = true;
    
    // Configure columns
    series.columns.template.width = am4core.percent(60);
    series.columns.template.tooltipText = "[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}";
    
    // Add label
    var labelBullet = series.bullets.push(new am4charts.LabelBullet());
    labelBullet.label.text = "{valueY}";
    labelBullet.locationY = 0.5;
    
    return series;
  }
  
  createSeries("false", "falsche Vorhersagen");
  createSeries("true", "korrekte Vorhersagen");
  
  
  // Legend
  chart3.legend = new am4charts.Legend();




/* curvedColumn */

var chart4 = am4core.create("curvedColumn", am4charts.XYChart);
chart4.hiddenState.properties.opacity = 0; // this makes initial fade in effect

chart4.data = [{
  "test": "Optimum",
  "value": 1
},{
  "test": "Test 1",
  "value": 0.9835092631108617
}, {
  "test": "Test 2",
  "value": 0.9822982022892811
}];


var categoryAxis = chart4.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.renderer.grid.template.location = 0;
categoryAxis.dataFields.category = "test";

var valueAxis = chart4.yAxes.push(new am4charts.ValueAxis());
valueAxis.extraMin = 0.5;
valueAxis.extraMax = 0.1; 

var series = chart4.series.push(new am4charts.CurvedColumnSeries());
series.dataFields.categoryX = "test";
series.dataFields.valueY = "value";
series.tooltipText = "{valueY.value}"
series.columns.template.strokeOpacity = 0;

series.columns.template.fillOpacity = 0.75;

var hoverState = series.columns.template.states.create("hover");
hoverState.properties.fillOpacity = 1;
hoverState.properties.tension = 0.4;

chart4.cursor = new am4charts.XYCursor();

// Add distinctive colors for each column using adapter
series.columns.template.adapter.add("fill", (fill, target) => {
  return chart4.colors.getIndex(target.dataItem.index);
});


/* Standardabweichung & Mittelwert*/


var sigma1 = am4core.create("sigma1", am4charts.XYChart);

sigma1.data = [{
  "x": 1,
  "y": 18.78,
  "errorY": 31.79,
  "text": "Baseline"
}, {
  "x": 2,
  "y": 16.07,
  "errorY": 8.9,
  "text": "Taxi Zeit"
}, {
  "x": 3,
  "y": 19.34,
  "errorY": 33.22,
  "text": "Beste Vorhersage"
}];

var valueAxisX = sigma1.xAxes.push(new am4charts.ValueAxis());
var valueAxisY = sigma1.yAxes.push(new am4charts.ValueAxis());
valueAxisY.min = -15;
valueAxisY.max = 65;
valueAxisX.step = 1;
var series = sigma1.series.push(new am4charts.LineSeries());
series.dataFields.valueX = "x";
series.dataFields.valueY = "y";
series.strokeOpacity = 0;

var errorBulletY = series.bullets.create(am4charts.ErrorBullet);
errorBulletY.isDynamic = true;
errorBulletY.strokeWidth = 4;
errorBulletY.tooltipText = "{text}: Mittelwert: {valueY.value} Abweichung: {errorY}";

// adapter adjusts height of a bullet
errorBulletY.adapter.add("pixelHeight", function (pixelHeight, target) {
  var dataItem = target.dataItem;

  if (dataItem) {
    var value = dataItem.valueY;
    var errorTopValue = value + dataItem.dataContext.errorY;
    var errorTopY = valueAxisY.valueToPoint(errorTopValue).y;

    var errorBottomValue = value - dataItem.dataContext.errorY;
    var errorBottomY = valueAxisY.valueToPoint(errorBottomValue).y;

    return Math.abs(errorTopY - errorBottomY);
  }
  return pixelHeight;
})

var errorBulletX = series.bullets.create(am4charts.ErrorBullet);
errorBulletX.tooltipText = "{text}: Mittelwert: {valueY.value} Abweichung: {errorY}";
errorBulletX.isDynamic = true;
errorBulletX.strokeWidth = 4;
errorBulletX.rotation = 90; // need to rotate, as error bullet is vertical

var circle = errorBulletX.createChild(am4core.Circle);
circle.radius = 8;
circle.fill = am4core.color("#ffffff");

// adapter adjusts height of a bullet
errorBulletX.adapter.add("pixelHeight", function (pixelHeight, target) {
  var dataItem = target.dataItem;

  if (dataItem) {
    var value = dataItem.valueY;
    var errorTopValue = value + dataItem.dataContext.errorX / 2;
    var errorTopX = valueAxisX.valueToPoint(errorTopValue).x;

    var errorBottomValue = value - dataItem.dataContext.errorX / 2;
    var errorBottomX = valueAxisX.valueToPoint(errorBottomValue).x;

    return Math.abs(errorTopX - errorBottomX);
  }
  return pixelHeight;
})

/* Balkendiagramme*/
 // Create series
 function createSeries2(field, name, chart) {
  var series = chart.series.push(new am4charts.ColumnSeries());
  series.dataFields.valueX = field;
  series.dataFields.categoryY = "test";
  series.name = name;
  series.columns.template.tooltipText = "{name}: [bold]{valueX}[/]";
  series.columns.template.height = am4core.percent(100);
  series.sequencedInterpolation = true;

  var valueLabel = series.bullets.push(new am4charts.LabelBullet());
  valueLabel.label.text = "{valueX}";
  valueLabel.label.horizontalCenter = "left";
  valueLabel.label.dx = 10;
  valueLabel.label.hideOversized = false;
  valueLabel.label.truncate = false;
}

/* regression*/
  
  // Create chart instance
  var regression = am4core.create("regression", am4charts.XYChart);
  
  // Add data
  regression.data = [{
    "test": "Fall 1",
    "mean": 19.067,
    "median": 11.19,
    "time": 151.71, 
    "estim": 200
  }, {
    "test": "Fall 2",
    "mean": 21.38,
    "median": 9.4,
    "time": 15.83,
    "estim": 10
  }, {
    "test": "Fall 3",
    "mean": 19.08,
    "median": 10.06,
    "time": 1216.86,
    "estim": 200
  }];
  
  // Create axes
  var regressionCategoryAxis = regression.yAxes.push(new am4charts.CategoryAxis());
  regressionCategoryAxis.dataFields.category = "test";
  regressionCategoryAxis.numberFormatter.numberFormat = "#";
  regressionCategoryAxis.renderer.inversed = true;
  regressionCategoryAxis.renderer.grid.template.location = 0;
  regressionCategoryAxis.renderer.cellStartLocation = 0.1;
  regressionCategoryAxis.renderer.cellEndLocation = 0.9;
  
  var  regressionValueAxis = regression.xAxes.push(new am4charts.ValueAxis()); 
  regressionValueAxis.renderer.opposite = true;
  
  createSeries2("mean", "Mittelwert", regression);
  createSeries2("median", "Median", regression);
  createSeries2("time", "Zeit", regression);
  createSeries2("estim", "Estimator", regression);
  
  regression.legend = new am4charts.Legend();

/* estimator*/

   // Create chart instance
   var estimator = am4core.create("estimator", am4charts.XYChart);
  
   // Add data
   estimator.data = [{
     "test": "10",
     "mean": 19.13,
     "median": 11.13,
     "time": 7.62, 
     "estim": 10
   }, {
     "test": "50",
     "mean": 19.06,
     "median": 11.18,
     "time": 37.53,
     "estim": 50
   }, {
     "test": "100",
     "mean": 19.08,
     "median": 11.19,
     "time": 73.72,
     "estim": 100
   }, {
    "test": "200",
    "mean": 19.06,
    "median": 11.19,
    "time": 151.71,
    "estim": 200
  }];
   
   // Create axes
   var estimatorCategoryAxis = estimator.yAxes.push(new am4charts.CategoryAxis());
   estimatorCategoryAxis.dataFields.category = "test";
   estimatorCategoryAxis.numberFormatter.numberFormat = "#";
   estimatorCategoryAxis.renderer.inversed = true;
   estimatorCategoryAxis.renderer.grid.template.location = 0;
   estimatorCategoryAxis.renderer.cellStartLocation = 0.1;
   estimatorCategoryAxis.renderer.cellEndLocation = 0.9;
   estimatorCategoryAxis.title.text = "Estimator";
   estimatorCategoryAxis.title.fontSize = 16;
   
   var  estimatorValueAxis = estimator.xAxes.push(new am4charts.ValueAxis()); 
   estimatorValueAxis.renderer.opposite = true;
   
   createSeries2("mean", "Mittelwert", estimator);
   createSeries2("median", "Median", estimator);
   createSeries2("time", "Zeit", estimator);
   createSeries2("estim", "Estimator", estimator);
   
   estimator.legend = new am4charts.Legend();